package com.maocq.scala

import com.maocq.scala.clases.{Dog, Persona, Saludos}

/**
  * Created by User on 2016.
  */
object Main {

  def main(args: Array[String]): Unit = {
    println("Hello")
  }


  /**
    * Declaracion de variables
    */
  def variables(): Unit = {
    val inmutable = "Soy inmutable"

    var mutable = "=)"
    mutable = "Soy mutable"
  }

  /**
    * Instanciación de clases
    */
  def clases(): Unit = {
    val persona = new Persona("Mauricio", "Carmona", 28)
    persona.saludar()
  }

  /**
    * Options
    */
  def options(): Unit = {
    val someValue: Option[String] = Some("I am wrapped in something")
    val emptyValue: Option[String] = None
    emptyValue getOrElse "No value"   // "No value"
    emptyValue.isEmpty                // true

    val noValue: Option[Double] = None
    val value = noValue match {
      case Some(v) ⇒ v
      case None ⇒ 0.0
    }
    value // 0.0f
  }

  /**
    * Objetos (Singleton)
    */
  def objetos() = {
    val x = Saludos
    println(x.english)
    println(Saludos.espanol)
  }

  /**
    * Método void
    */
  def saludar(): Unit = {
    println("Hola =)")
  }

  /**
    * Método que recibe y retorna un String
    */
  def getSaludo(nombre: String): String = {
    val saludo = s"Hola $nombre"
    saludo
  }

  /**
    * Estructuras de control
    */
  def estructurasControl() = {
    val check: Boolean = true

    if (check)
      println("True")
    else
      println("False")

    var x = 0
    while (x < 5) {
      //En una sola linea seria de esta forma
      //println(x); x += 1
      println(x)
      x += 1
    }

    for (i <- 5 to 10) {
      println(i)
    }

    // Foreach
    val numeros = List(4, 3, 2, 1)
    for (numero <- numeros) {
      println(numero)
    }
  }
    /**
    * Foreach
    */
  def recorrer() = {

    val lista = List(1, 2, 3, 4, 5)
    lista foreach {
      i => println("Int: " + i)
    }

    val stateCapitals = Map(
      "Alabama" -> "Montgomery",
      "Alaska" -> "Juneau",
      "Wyoming" -> "Cheyenne")

    stateCapitals foreach {
      case (k, v) => println(k + ": " + v)
    }

  }

  /**
    * Tuplas
    */
  def tuplas() = {
    val tupla = (1, "Hello", Console)
    println(tupla._3)
  }


  /**
    * Funciones de orden superior
    */
  def lambda = (x: Int) ⇒ x + 1

  def funcionesOrdenSuperior() = {
    println(lambda(1))
  }

  /**
    * Listas
    */
  def listas() = {
    //val listaString:List[String] = List("Hola", "Hi", "Hallo")
    val listaString = List("Hola", "Hi", "Hallo")
    val listaInt = List(1, 2, 3)

    listaInt.head // 1
    listaInt.tail // List(2 ,3)
    listaInt.isEmpty // false
    listaInt(0) // 1
    listaInt.length // 3
    listaInt.reverse // List(3, 2, 1)

    listaInt.filterNot(v => v == 2) // List(1, 3)
    listaInt.filter(v => v % 2 == 0) // List(2)
    listaInt.filter(_ % 2 == 0) // List(2)
    listaInt.map(_ * 2) // List(2, 4, 6)

    listaInt.reduceLeft(_ + _) // 6
    listaInt.foldLeft(2)(_ + _) // 8

    val a = (1 to 5).toList // List(1,2,3,4,5)
    val lista = List(2, 3)
    1 :: lista // List(1, 2, 3)

      List(2, 3) ++ List(4) // List(2, 3, 4)
  }

  /**
    * Mapas
    */
  def mapas() = {
    val myMap = Map("MI" -> "Michigan", "OH" -> "Ohio", "WI" -> "Wisconsin", "IA" -> "Iowa")
    myMap.size // 4

    // +
    myMap + ("IL" → "Illinois") // Retorna nuevo Map
    // -
    myMap - "MI" // Retorna nuevo Map si en elemento "MI"

    myMap.contains("MI") // true
    myMap.values // MapLike(Michigan, Ohio, Wisconsin, Iowa)
    myMap.keys // Set(MI, OH, WI, IA)

    myMap.head // (MI,Michigan)
    myMap.last // (IA,Iowa)
    myMap("MI") // Michigan
  }

  /**
    * Sets  (No permite elementos repetidos)
    */
  def sets() = {
    val mySet = Set("Michigan", "Ohio", "Wisconsin", "Iowa")
    mySet.size // 4

    // +
    mySet + "Illinois" // Retorna nuevo Set
    // -
    mySet - "Michigan"

    mySet.contains("Ohio") // true
    mySet.contains("Illinois") // false
    mySet("Ohio") // true

    // Nota: Permite operaciones de conjuntos
  }

  /**
    * Array
    */
  def arrays() = {
    val arrayString = Array("Zara", "Nuha", "Ayan")
    for (x <- arrayString) {
      println(x)
    }
    arrayString(2) = "Avan"
    for (i <- 0 to (arrayString.length - 1)) {
      println(arrayString(i))
    }
  }

  /**
    * Formateo
    * Puedo declarar la funcion sin parentesis pero al llamarla
    * se hace sin parentesis
    */
  def formateo = {
    val s = "Hello World"
    println("Application %s".format(s)) // "Application Hello World"
  }

  /**
    * Pattern Matching
    */
  def patternMatching() = {
    val foodItem = "porridge"

    def goldilocks(expr: Any) = expr match {
      case (`foodItem`, _) => "eating"
      case ("chair", "Mama") => "sitting"
      case ("bed", "Baby") => "sleeping"
      case _ => "what?"
    }

    goldilocks(("porridge", "Papa")) // "eating"
    goldilocks(("beer", "Cousin")) // "what?"
  }

  /**
    * Case Classes
    * Se instancia sin new
    */
  def caseClass() = {
    val dog = Dog("Scooby", "Doberman")

    dog.name // "Scooby"
    dog.breed // "Doberman"

    dog.name = "Scooby Doo"
    dog.isInstanceOf[Serializable] // true
  }

  /**
    * Rangos
    */
  def rangos() = {
    val someNumbers = Range(0, 10)
    someNumbers(0) //0
  }

  /**
    * Funciones parcialmente Aplicadas
    */
  def funcionesParcialmenteAplicadas() = {

    def cat1(s1: String)(s2: String) = s1 + s2
    val hello = cat1("Hello ") _
    hello("World!") // Hello World!


    def multiply(x: Int, y: Int) = x * y
    //(multiply _).isInstanceOf[Function2[_, _, _]]
    val multiplyCurried = (multiply _).curried
    multiply(4, 5) // 20
    multiplyCurried(3)(2) // 6
    val multiplyCurriedFour = multiplyCurried(4)
    multiplyCurriedFour(2) // 8
    multiplyCurriedFour(4) // 16
  }

  /**
    * Funciones parciales
    */
  def funcionesParciales() = {
    // Si es par
    val doubleEvens: PartialFunction[Int, Int] = {
      case x if (x % 2) == 0 ⇒ x * 2
    }
    // Si no es par
    val tripleOdds: PartialFunction[Int, Int] = {
      case x if (x % 2) != 0 ⇒ x * 3
    }
    // Va verificando de derecha a izquierda
    // La primera en cumplirse es la que se ejecuta
    val whatToDo = doubleEvens orElse tripleOdds

    // andThen
    //val addFive = (x: Int) ⇒ x + 5
    //val whatToDo = doubleEvens orElse tripleOdds andThen addFive

    whatToDo(3) //9
    whatToDo(4) //8
  }

  /**
    * Implicitos
    */
  def implicitos() = {
    class KoanIntWrapper(val original: Int) {
      def isOdd = original % 2 != 0
    }
    implicit def thisMethodNameIsIrrelevant(value: Int) = new KoanIntWrapper(value)

    19.isOdd // true
  }

  /**
    * Traits
    */
  def traits() = {
    case class Event(name: String)

    trait EventListener {
      def listen(event: Event): String
    }

    class MyListener extends EventListener {
      def listen(event: Event): String = {
        event match {
          case Event("Moose Stampede") => "An unfortunate moose stampede occurred"
          case _ => "Nothing of importance occurred"
        }
      }
    }

    val evt = Event("Moose Stampede")
    val myListener = new MyListener
    myListener.listen(evt) //"An unfortunate moose stampede occurred"
  }

  /**
    * For Expressions
    */
  def forExpressions() = {

    val lista = List(1, 2, 3)
    val s = for (v <- lista) yield v
    // List(1, 2, 3)

    val dogBreeds = List("Doberman", "Yorkshire Terrier", "Dachshund",
      "Scottish Terrier", "Great Dane", "Portuguese Water Dog")
    val filteredBreeds = for {
      breed <- dogBreeds
      if breed.contains("Terrier")
    } yield breed
    filteredBreeds // List(Yorkshire Terrier, Scottish Terrier)

    val xValues = Range(1, 5)
    val yValues = Range(1, 3)
    val coordinates = for {
      x <- xValues
      y <- yValues
    } yield (x, y)

    coordinates // Vector((1,1), (1,2), (2,1), (2,2), (3,1), (3,2), (4,1), (4,2))
  }

  /**
    * Prefijo y Sufijo
    */
  def prefijoYSufijo() = {
    class Stereo {
      def unary_+ = "on"
      def unary_- = "off"
    }

    val stereo = new Stereo
    (+stereo)  // "on"
  }

  /**
    * Secuencias y matrices
    */
  def secuenciasYMatrices() = {
    // Array a Lista
    val array = Array(1, 2, 3)
    val lista = array.toList // List(1, 2, 3)
  }

  /**
    * Iterables
    */
  def iterables() = {

    // Iteración
    val list = List(3, 5, 9, 11, 15, 19, 21)
    val it = list.iterator
    if (it.hasNext) {
      it.next //3
    }

    // Agrupar
    val list2 = List(3, 5, 9, 11, 15, 19, 21, 24, 32)
    val it2 = list grouped 3
    it2.next() // List(3, 5, 9)

    // Ultimos elementos
    val list3 = List(3, 5, 9, 11, 15, 19, 21, 24, 32)
        // Ultimos 3
    (list3 takeRight 3) // List(21, 24, 32)

    // Emparejar
    val xs = List(3, 5, 9)
    val ys = List("Bob", "Ann", "Stella")
    (xs zip ys) // List[(Int, String)] = List((3,Bob), (5,Ann), (9,Stella))
  }

  /**
    * Transversales
    */
  def transversales() = {
    val set = Set(1, 3, 4, 6)
    val result = set.map(_ * 4)
    // Set(4, 12, 16, 24)

    val list = List(List(1), List(2, 3, 4), List(5, 6, 7), List(8, 9, 10))
    list.flatten
    // List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

    val list1 = List(List(1), List(2, 3, 4), List(5, 6, 7), List(8, 9, 10))
    val result1 = list1.flatMap(_.map(_ * 4))
    // List(4, 8, 12, 16, 20, 24, 28, 32, 36, 40)

    val list2 = List(10, 19, 45, 1, 22)
    list2.find(_ % 2 != 0)
    // Some(19)

    val list3 = List(10, 19, 45, 1, 22)
    list3.slice(1, 3)
    // List(19, 45)

    val list4 = List(10, 19, 45, 1, 22)
    list4.take(3)
    // List(10, 19, 45)

    val list5 = List(87, 44, 5, 4, 200, 10, 39, 100)
    list5.takeWhile(_ < 100)
    // List(87, 44, 5, 4)

    val list6 = List(87, 44, 5, 4, 200, 10, 39, 100)
    val result6 = list6 forall (_ < 100)
    // false       - Verifica si es válido para todos los elementos

    val list7 = List(87, 44, 5, 4, 200, 10, 39, 100)
    val result7 = list7 count (_ < 100)
    // 6           - Contará el número de elementos
  }

  /**
    * Nombres y argumentos por defecto
    */
  def porDefecto() = {

    def printName(first: String, last: String) = {
      println(first + " " + last)
    }
    printName("John", "Smith")
    printName(last = "Smith", first = "John")
  }

  /**
    * Enumeraciones
    */
  def enumeraciones() = {
    object Planets extends Enumeration {
      val Mercury = Value
      val Venus = Value
      val Earth = Value
      val Mars = Value
      val Jupiter = Value
      val Saturn = Value
      val Uranus = Value
      val Neptune = Value
      val Pluto = Value
    }

    Planets.Mars.id // 3
    Planets.Mars.toString // "Mars"
  }
}

