package com.maocq.scala.clases

/**
  * Created by User on 16/06/2016.
  */
class Persona(nombrePersona: String, apellidoPersona: String, private val edadPersona: Int) {
  def nombre = nombrePersona

  //val nombre = nombrePersona
  def apellido = apellidoPersona

  def edad = edadPersona

  def saludar(): Unit = {
    val saludo = s"Hola mi nombre es $nombre $apellido"
    println(saludo)
  }

  override def toString = s"Persona($nombre, $apellido)"
}
