package com.maocq.scala.clases

/**
  * name es una propiedad Mutable
  * breed tiene un valor por defecto
  * Created by User on 16/06/2016.
  */
case class Dog(var name: String, breed: String = "mestizo")
